/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin;

import java.util.HashMap;
import java.util.Map;

public class AdeHelper {

	public static Map<String, String> parseOutput(String output) {
		Map<String, String> result = new HashMap<String, String>();
		for (String line: output.trim().split("\n")) {
			if (line.contains(":")) {
				String[] parts = line.split(":", 2);
				if (parts.length < 2)
					continue;
				result.put(parts[0], parts[1]);
			}
		}
		return result;
	}

}
